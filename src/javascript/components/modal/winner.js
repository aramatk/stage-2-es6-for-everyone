import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import App from '../../app';

export function showWinnerModal(fighter) {
  const winnerTitle = `Winner is ${fighter.name}`;
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterImage = createFighterImage(fighter);
  bodyElement.append(fighterImage);
  const onClose = () => {
    const arenaRoot = document.getElementsByClassName('arena___root')[0];
    arenaRoot?.remove();
    new App();
  };
  showModal({ title: winnerTitle, bodyElement: bodyElement, onClose: onClose });
}
