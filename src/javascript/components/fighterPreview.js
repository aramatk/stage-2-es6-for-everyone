import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (fighter) {
    const nameElement = createTextInfoElement('Name', fighter.name);
    const healthElement = createTextInfoElement('Health', fighter.health);
    const attackElement = createTextInfoElement('Attack', fighter.attack);
    const defenseElement = createTextInfoElement('Defense', fighter.defense);
    const imageElement = createFighterImage(fighter);
    fighterElement.append(nameElement, healthElement, attackElement, defenseElement, imageElement);
  }

  return fighterElement;
}

function createTextInfoElement(property, value) {
  const nameElement = createElement({ tagName: 'span', className: 'preview-text' });
  nameElement.innerText = `${property}: ${value}`;
  ;

  return nameElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
