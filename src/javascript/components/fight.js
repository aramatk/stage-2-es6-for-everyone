import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const keyMap = new Map();
  initKeyMap(keyMap, Object.values(controls));
  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;
  firstFighter.didCriticalHit = false;
  secondFighter.didCriticalHit = false;
  firstFighter.isWinner = false;
  secondFighter.isWinner = false;

  document.addEventListener('keydown', function(event) {
    if (keyMap.has(event.code)) {
      keyMap.set(event.code, true);
      if (controls.PlayerOneAttack === event.code) {
        doAttack(keyMap, firstFighter, secondFighter, 'One', 'Two');
      } else if (controls.PlayerTwoAttack === event.code) {
        doAttack(keyMap, secondFighter, firstFighter, 'Two', 'One');
      } else if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
        doCriticalHit(keyMap, firstFighter, secondFighter, 'One', 'Two');
      } else if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
        doCriticalHit(keyMap, secondFighter, firstFighter, 'Two', 'One');
      }
    }
  });

  document.addEventListener('keyup', function(event) {
    if (keyMap.has(event.code)) {
      keyMap.set(event.code, false);
    }
  });

  return new Promise((resolve) => {
    (function waitForWinner() {
      const winner = getWinner(firstFighter, secondFighter);
      if (winner) {
        resolve(winner);
      }
      setTimeout(waitForWinner, 1);
    })();
  });
}

function getWinner(firstFighter, secondFighter) {
  if (firstFighter.isWinner) {
    return firstFighter;
  } else if (secondFighter.isWinner) {
    return secondFighter;
  }
}

function doAttack(keyMap, attacker, defender, attackerNumber, defenderNumber) {
  if (keyMap.get(controls[`Player${attackerNumber}Attack`]) && !keyMap.get(controls[`Player${attackerNumber}Block`])) {
    if (!keyMap.get(controls[`Player${defenderNumber}Block`])) {
      defender.currentHealth -= getDamage(attacker, defender);
      decreaseHealthBar(defenderNumber === 'One' ? 'left' : 'right', defender);
      if (defender.currentHealth <= 0 && !defender.isWinner) {
        attacker.isWinner = true;
      }
    }
  }
}

function doCriticalHit(keyMap, attacker, defender, attackerNumber, defenderNumber) {
  const codeCombinations = controls[`Player${attackerNumber}CriticalHitCombination`];
  if (isAllPressed(keyMap, codeCombinations) && !attacker.didCriticalHit) {
    attacker.didCriticalHit = true;
    defender.currentHealth -= 2 * attacker.attack;
    setTimeout(() => {
      attacker.didCriticalHit = false;
    }, 10000);
    decreaseHealthBar(defenderNumber === 'One' ? 'left' : 'right', defender);
    if (defender.currentHealth <= 0 && !defender.isWinner) {
      attacker.isWinner = true;
    }
  }
}

function isAllPressed(keyMap, codeCombinations) {
  for (let key of codeCombinations) {
    if (!keyMap.get(key)) {
      return false;
    }
  }
  return true;
}

function decreaseHealthBar(position, fighter) {
  fighter.currentHealth = fighter.currentHealth < 0 ? 0 : fighter.currentHealth;
  const healthBar = document.getElementById(`${position}-fighter-indicator`);
  healthBar.style.width = `${fighter.currentHealth * 100 / fighter.health}%`;
}

function initKeyMap(keyMap, values) {
  for (let value of values) {
    if (Array.isArray(value)) {
      initKeyMap(keyMap, value);
    } else {
      keyMap.set(value, false);
    }
  }
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  damage = damage < 0 ? 0 : damage;
  return damage;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}